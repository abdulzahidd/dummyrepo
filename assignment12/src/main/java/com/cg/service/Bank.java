/**************************************************************************************
											Capgemini Engineering Proprietary

This source code is the sole property of Capgemini Engineering Proprietary. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from Capgemini Engineering.

File Name             : Bank.java
Principal Author      : Abdul Zahid
Subsystem Name        : 
Module Name           : 
Date of First Release : Jan 17, 2022 11:16:32 PM
Author                : Abdul Zahid
Description           : 


Change History

Version               : 1.0
Date (DD/MM/YYYY)     : Jan 17, 2022
Modified by           : Abdul Zahid
Description of Change : Initial Version 

 *****************************************************************************************/
package com.cg.service;

/**
 *@see Bank.java
 *@version 1.0
 *@author Abdul Zahid
 *@since Jan 17, 2022 11:16:32 PM
 */
public interface Bank {
	
	int interestRate();

	int loanRate();
}
