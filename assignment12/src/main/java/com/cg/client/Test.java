/**************************************************************************************
											Capgemini Engineering Proprietary

This source code is the sole property of Capgemini Engineering Proprietary. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from Capgemini Engineering.

File Name             : Test.java
Principal Author      : Abdul Zahid
Subsystem Name        : 
Module Name           : 
Date of First Release : Jan 18, 2022 7:54:00 PM
Author                : Abdul Zahid
Description           : 


Change History

Version               : 1.0
Date (DD/MM/YYYY)     : Jan 18, 2022
Modified by           : Abdul Zahid
Description of Change : Initial Version 

 *****************************************************************************************/
package com.cg.client;

import java.util.Scanner;

import com.cg.service.Bank;
import com.cg.serviceimpl.Factory;

/**
 * @see Test.java
 * @version 1.0
 * @author Abdul Zahid
 * @since Jan 18, 2022 7:54:00 PM
 */
public class Test {
	public static void main(String[] args) {
		// Scanner class used to get the input from the user
		Scanner sc = new Scanner(System.in);
		System.out.println("For information Please Enter the bank name.");
		String bankName = sc.next();
//		calling factoryMethod using Bank Class 
		Bank bank = Factory.factoryMethod(bankName);
		System.out.println(bankName+" bank with interest rate "+bank.interestRate()+"% and with loan rate "+bank.loanRate()+"%");

	}
}
