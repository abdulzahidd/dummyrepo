/**************************************************************************************
											Capgemini Engineering Proprietary

This source code is the sole property of Capgemini Engineering Proprietary. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from Capgemini Engineering.

File Name             : Axis.java
Principal Author      : Abdul Zahid
Subsystem Name        : 
Module Name           : 
Date of First Release : Jan 17, 2022 11:17:55 PM
Author                : Abdul Zahid
Description           : 


Change History

Version               : 1.0
Date (DD/MM/YYYY)     : Jan 17, 2022
Modified by           : Abdul Zahid
Description of Change : Initial Version 

 *****************************************************************************************/
package com.cg.serviceimpl;

import com.cg.service.Bank;

/**
 * @see Axis.java
 * @version 1.0
 * @author Abdul Zahid
 * @since Jan 17, 2022 11:17:55 PM
 */
public class Axis implements Bank {

	/* (non-Javadoc)
	 * @see com.cg.service.Bank#interestRate()
	 */
	public int interestRate() {
		return 4;
	}

	/* (non-Javadoc)
	 * @see com.cg.service.Bank#loanRate()
	 */
	public int loanRate() {
		return 10;
	}

}
