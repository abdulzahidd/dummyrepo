/**************************************************************************************
											Capgemini Engineering Proprietary

This source code is the sole property of Capgemini Engineering Proprietary. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from Capgemini Engineering.

File Name             : Factory.java
Principal Author      : Abdul Zahid
Subsystem Name        : 
Module Name           : 
Date of First Release : Jan 18, 2022 7:44:22 PM
Author                : Abdul Zahid
Description           : 


Change History

Version               : 1.0
Date (DD/MM/YYYY)     : Jan 18, 2022
Modified by           : Abdul Zahid
Description of Change : Initial Version 

 *****************************************************************************************/
package com.cg.serviceimpl;

import com.cg.service.Bank;

/**
 * @see Factory.java
 * @version 1.0
 * @author Abdul Zahid
 * @since Jan 18, 2022 7:44:22 PM
 */
public class Factory {

	/**
	 *@see factoryMethod
	 *@version 1.0
	 *@author Abdul Zahid
	 *@since 19-Jan-2022 11:12:21 PM
	 * @param bankName
	 * @return
	 */
	static public Bank factoryMethod(String bankName) {
		if (bankName.equalsIgnoreCase("Axis")) {
			return new Axis();
		} else if (bankName.equalsIgnoreCase("HDFC")) {
			return new HDFC();
		} else if (bankName.equalsIgnoreCase("ICICI")) {
			return new ICICI();
		} else
			return null;
	}

}
