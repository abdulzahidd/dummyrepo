/**************************************************************************************
											Capgemini Engineering Proprietary

This source code is the sole property of Capgemini Engineering Proprietary. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from Capgemini Engineering.

File Name             : Hdfc.java
Principal Author      : Abdul Zahid
Subsystem Name        : 
Module Name           : 
Date of First Release : Jan 18, 2022 7:40:19 PM
Author                : Abdul Zahid
Description           : 


Change History

Version               : 1.0
Date (DD/MM/YYYY)     : Jan 18, 2022
Modified by           : Abdul Zahid
Description of Change : Initial Version 

 *****************************************************************************************/
package com.cg.serviceimpl;

import com.cg.service.Bank;

/**
 * @see HDFC.java
 * @version 1.0
 * @author Abdul Zahid
 * @since Jan 18, 2022 7:40:19 PM
 */
public class HDFC implements Bank {

	/* (non-Javadoc)
	 * @see com.cg.service.Bank#interestRate()
	 */
	@Override
	public int interestRate() {

		return 3;
	}

	/* (non-Javadoc)
	 * @see com.cg.service.Bank#loanRate()
	 */
	@Override
	public int loanRate() {

		return 11;
	}

}
